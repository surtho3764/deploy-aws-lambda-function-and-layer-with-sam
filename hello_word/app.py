# 指定模型的訓練方式
# 優化器使用rmsprop優化器
# 損失函數選擇sparse_categorical_crossentropy
# 驗證損確度使用accuracy
'''
model.compile(optimizer='rmsprop',
              loss = 'sparse_categorical_crossentropy',
              metrics = ['accuracy'])
'''

